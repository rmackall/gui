import acm.program.*;
import acm.graphics.*;
import javax.swing.*;
import java.awt.event.ActionEvent;
import java.util.Stack;


public class GUI extends Program
{
    JTextField infixIn;
    JTextField infixOutput;
    JTextField totalAwnser;
    
    
   
    public GUI()
    {
        start();
        setSize(400, 225);
    }
    
    public void init()
    {
        // Create a canvas and add it to the window
        GCanvas canvas = new GCanvas();
        add(canvas);
        
        JLabel postLabel = new JLabel("Post");
        JLabel infixLabel = new JLabel("Infix");
        JLabel anwserLabel = new JLabel("Awnser");
        canvas.add(postLabel, 20, 20);
        canvas.add(infixLabel, 20, 60);
        canvas.add(anwserLabel, 20, 100);
        
        
        infixIn = new JTextField();
        infixOutput = new JTextField();
        totalAwnser = new JTextField();
        canvas.add(infixIn, 70, 20);
        canvas.add(infixOutput, 70, 60);
        canvas.add(totalAwnser, 70, 100);
        infixIn.setSize(100, 20);
        infixOutput.setSize(100, 20);
        totalAwnser.setSize(100, 20);
        
        JButton numberOne = new JButton("1");
        JButton numberTwo = new JButton("2");
        JButton numberThree = new JButton("3");
        JButton numberFour = new JButton("4");
        JButton numberFive = new JButton("5");
        JButton numberSix = new JButton("6");
        JButton numberSeven = new JButton("7");
        JButton numberEight = new JButton("8");
        JButton numberNine = new JButton("9");
        JButton numberZero = new JButton("0");
        JButton sqrt = new JButton("sqrt");
        JButton power = new JButton("^");
        JButton plus = new JButton("+");
        JButton minus = new JButton("-");
        JButton multi = new JButton("*");
        JButton div = new JButton("/");
        
        canvas.add(sqrt, 313, 20);
        canvas.add(power, 300, 45);
        canvas.add(plus, 300, 70);
        canvas.add(minus, 300, 95);
        canvas.add(multi, 340, 45);
        canvas.add(div, 340, 70);
        
        canvas.add(numberOne, 175, 20);
        canvas.add(numberTwo, 215, 20);
        canvas.add(numberThree, 255, 20);
        canvas.add(numberFour, 175, 45);
        canvas.add(numberFive, 215, 45);
        canvas.add(numberSix, 255, 45);
        canvas.add(numberSeven, 175, 70);
        canvas.add(numberEight, 215, 70);
        canvas.add(numberNine, 255, 70);
        canvas.add(numberZero, 215, 95);
        
        
        
        
        JButton convertButton = new JButton("Convert");
        JButton clearButton = new JButton("Clear");
        canvas.add(convertButton, 20, 125);
        canvas.add(clearButton, 120, 125);
        
        addActionListeners();
    }
    
    public void actionPerformed(ActionEvent e)
    {
        //System.out.println(e.getActionCommand());
               
        if (e.getActionCommand().equals("Convert"))
        {
            // Get value from mphInput
            String infixStr = infixIn.getText();
            String infix = (infixStr);
            // Create Mph2Mps converter
            SY n = new SY("" + infix);
                    
            // Call convert
            String postfix = n.convert();
            
            // Put result into mpsOutput
            infixOutput.setText("" + postfix);
            
            //Awnser 
            Postfix a = new Postfix(postfix);
            double awn = a.eval();
            totalAwnser.setText("" + awn);
            
        }
        else if (e.getActionCommand().equals("Clear"))
        {
            infixIn.setText("");
            infixOutput.setText("");
            totalAwnser.setText("");
        }
        
        
    }
}