import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * The test class SYTest.
 *
 * @author  (your name)
 * @version (a version number or a date)
 */
public class SYTest
{
    /**
     * Default constructor for test class SYTest
     */
    public SYTest()
    {
    }

    /**
     * Sets up the test fixture.
     *
     * Called before every test case method.
     */
    @Before
    public void setUp()
    {
    }

    /**
     * Tears down the test fixture.
     *
     * Called after every test case method.
     */
    @After
    public void tearDown()
    {
    }
    
    @Test
    public void testSY1()
    {
        SY s = new SY("2 + 3");
        assertEquals("2 3 +", s.convert());
    }
    
    @Test
    public void testSY2()
    {
        SY s = new SY("2 + 3 + 4");
        assertEquals("2 3 + 4 +", s.convert());
    }
    @Test
    public void testSY3()
    {
     SY s = new SY ("2 * ( 3 + 4 )");
     assertEquals ("2 3 4 + *", s.convert());
    } 
    
    @Test
    public void testSY4()
    {
     SY s = new SY ("2 * ( 3 + 4 * 5 ) + 6");
     assertEquals ("2 3 4 5 * + * 6 +", s.convert());
    } 
    
    @Test
    public void testSY5()
    {
     SY s = new SY ("2 * ( 3 + ( 4 * 5 ^ 6 ) + 7 )");
     assertEquals ("2 3 4 5 6 ^ * + 7 + *", s.convert());
    } 
    @Test
    public void testSY6()
    {
        SY s = new SY("4 * 4 sqrt");
        assertEquals("4 4 * sqrt", s.convert());
    }
    @Test
    public void testSY7()
    {
        SY s = new SY("4 + 4 ( 4 * 4 sqrt ) + 4");
        assertEquals("4 4 4 4 * sqrt + 4 +", s.convert());
    }
}