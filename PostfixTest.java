

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * The test class PostfixTest.
 *
 * @author  (your name)
 * @version (a version number or a date)
 */
public class PostfixTest
{
    /**
     * Default constructor for test class PostfixTest
     */
    public PostfixTest()
    {
    }

    /**
     * Sets up the test fixture.
     *
     * Called before every test case method.
     */
    @Before
    public void setUp()
    {
    }

    /**
     * Tears down the test fixture.
     *
     * Called after every test case method.
     */
    @After
    public void tearDown()
    {
    }
    
    @Test
    public void testEvalAdd()
    {
        Postfix p = new Postfix("2 3 +");
        assertEquals(5.0, p.eval(), 0.0001);
    }
    
    @Test
    public void testEvalSeveral()
    {
        Postfix p = new Postfix("2 3 + 4 *");
        assertEquals(20.0, p.eval(), 0.001);
    }
    
    @Test
    public void testEvalNumber()
    {
        Postfix p = new Postfix("6");
        assertEquals(6.0, p.eval(), 0.001);
    }
    
    @Test
    public void testEval2()
    {
        Postfix p = new Postfix("5 2 /");
        assertEquals(2.5, p.eval(), 0.001);
    }
    
    @Test
    public void testEval3()
    {
        Postfix p = new Postfix("7 8 -");
        assertEquals(-1.0, p.eval(), 0.001);
    }
    
    @Test
    public void testEval4()
    {
        Postfix p = new Postfix("5 2 / 0.5 - 8 *");
        assertEquals(16.0, p.eval(), 0.001);
    }
    
    @Test
    public void testEvalSqrt()
    {
        Postfix p = new Postfix("16 sqrt 2 / 4 +");
        assertEquals(6.0, p.eval(), 0.001);
    }
    
    @Test
    public void testEvalExponent()
    {
        Postfix p = new Postfix("2 3 ^");
        assertEquals(8.0, p.eval(), 0.001);
    }
    
    @Test
    public void testEvalExponent2()
    {
        Postfix p = new Postfix("2 3 ^ 2 ^");
        assertEquals(64.0, p.eval(), 0.001);
    }
    
    @Test
    public void testEvalExponentSqrt()
    {
        Postfix p = new Postfix("3 2 ^ sqrt");
        assertEquals(3.0, p.eval(), 0.001);
    }
}
